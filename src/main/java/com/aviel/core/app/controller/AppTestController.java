package com.aviel.core.app.controller;

import com.aviel.core.app.annotation.Login;
import com.aviel.core.app.annotation.LoginUser;
import com.aviel.core.app.model.AppUser;
import com.aviel.core.app.util.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author qhliu
 */
@RestController
@RequestMapping("/app")
public class AppTestController {

    @Login
    @GetMapping("/userInfo")
    public R userInfo(@LoginUser AppUser user) {
        return R.ok().put("user", user);
    }

    @Login
    @GetMapping("/userId")
    public R userInfo(@RequestAttribute("userId") String userId) {
        return R.ok().put("userId", userId);
    }

    @GetMapping("/notToken")
    public R notToken() {
        return R.ok().put("msg", "无需token也能访问");
    }

}

