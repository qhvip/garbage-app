package com.aviel.core.app.controller;

import com.aviel.core.app.exception.HandlerException;
import com.aviel.core.app.model.AppUser;
import com.aviel.core.app.service.UserService;
import com.aviel.core.app.util.R;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.UUID;

/**
 * @author qhliu
 */
@RestController
@RequestMapping("/app")
public class ApiRegisterController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public R register(
            @RequestParam(value = "phone") String phone,
            @RequestParam(value = "password") String password) {
        if (StringUtils.isBlank(phone)) {
            throw new HandlerException(HttpStatus.BAD_REQUEST.value(), "手机号不能为空");
        }
        if (StringUtils.isBlank(password)) {
            throw new HandlerException(HttpStatus.BAD_REQUEST.value(), "密码不能为空");
        }
        AppUser appUser = userService.queryByMobile(phone);
        if (appUser != null) {
            return R.error(HttpStatus.BAD_REQUEST.value(), "手机号已注册");
        }
        AppUser user = new AppUser();
        user.setUserId(UUID.randomUUID().toString().replace("-", ""));
        user.setPhone(phone);
        user.setPassword(DigestUtils.sha256Hex(password));
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        userService.save(user);
        return R.ok();
    }
}

