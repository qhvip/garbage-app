package com.aviel.core.app.controller;

import com.aviel.core.app.dto.CategorySecondaryDTO;
import com.aviel.core.app.service.CategoryService;
import com.aviel.core.app.util.R;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/app/category")
@Slf4j
public class ApiCategoryController {

    @Autowired
    private CategoryService categoryService;

    @PostMapping("/list")
    public R list(@RequestParam(value = "cid", required = true) Long cid) {
        List<CategorySecondaryDTO> categorySecondaryList = categoryService.queryCategorySecondaryList(cid);
        Map<String, Object> data = Maps.newHashMap();
        data.put("list", categorySecondaryList);
        Map<String, Object> map = Maps.newHashMap();
        map.put("data", data);
        return R.ok(map);
    }
}
