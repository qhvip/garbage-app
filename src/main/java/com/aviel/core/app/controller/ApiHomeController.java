package com.aviel.core.app.controller;

import com.aviel.core.app.annotation.Login;
import com.aviel.core.app.model.OrderProcess;
import com.aviel.core.app.service.OrderProcessService;
import com.aviel.core.app.util.R;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author qhliu
 */
@RestController
@RequestMapping("/app")
public class ApiHomeController {

    @Autowired
    private OrderProcessService orderProcessService;

    @Login
    @PostMapping("/index")
    public R list(@RequestAttribute("userId") String userId) {
        if (StringUtils.isBlank(userId)) {
            return R.error(HttpStatus.UNAUTHORIZED.value(), "用户未登录");
        }
        Map<String, Object> data = new HashMap<String, Object>();
        List<OrderProcess> processList = orderProcessService.queryList(userId);
        if (!CollectionUtils.isEmpty(processList)) {
            Map<Integer, Long> map = processList.stream().collect(Collectors.groupingBy(OrderProcess::getOrderStatus, Collectors.counting()));
            data.put("order_num", map.getOrDefault(1, 0L));
            data.put("process_num", map.getOrDefault(2, 0L) + map.getOrDefault(3, 0L));
            data.put("list", processList);
        } else {
            data.put("order_num", 0);
            data.put("process_num", 0);
            data.put("list", Collections.emptyList());
        }
        Map<String, Object> map = Maps.newHashMap();
        map.put("data", data);
        return R.ok(map);
    }
}