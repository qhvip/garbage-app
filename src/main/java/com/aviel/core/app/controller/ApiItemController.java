package com.aviel.core.app.controller;

import com.aviel.core.app.dto.ItemDTO;
import com.aviel.core.app.service.CategoryService;
import com.aviel.core.app.service.ItemService;
import com.aviel.core.app.util.R;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/app/item")
@Slf4j
public class ApiItemController {

    @Autowired
    private ItemService itemService;
    @Autowired
    private CategoryService categoryService;

    @PostMapping("/list")
    public R list(@RequestParam(value = "catId", required = true) Long catId) {
        categoryService.queryCategorySecondary(catId);
        List<ItemDTO> itemDTOList = itemService.queryByCatId(catId);
        Map<String, Object> data = Maps.newHashMap();
        data.put("list", CollectionUtils.isEmpty(itemDTOList) ? Collections.emptyList() : itemDTOList);
        Map<String, Object> map = Maps.newHashMap();
        map.put("data", data);
        return R.ok(map);
    }
}
