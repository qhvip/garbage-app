package com.aviel.core.app.controller;

import com.aviel.core.app.config.JwtConfig;
import com.aviel.core.app.exception.HandlerException;
import com.aviel.core.app.service.UserService;
import com.aviel.core.app.util.R;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author qhliu
 */
@RestController
@RequestMapping("/app")
public class ApiLoginController {
    @Autowired
    private UserService appUserService;
    @Autowired
    private JwtConfig jwtConfig;

    /**
     * 登录
     */
    @PostMapping("/login")
    public R login(
            @RequestParam(value = "phone") String phone,
            @RequestParam(value = "password") String password) {
        if (StringUtils.isBlank(phone)) {
            throw new HandlerException(HttpStatus.BAD_REQUEST.value(), "手机号不能为空");
        }
        if (StringUtils.isBlank(password)) {
            throw new HandlerException(HttpStatus.BAD_REQUEST.value(), "密码不能为空");
        }
        //用户登录
        String userId = appUserService.login(phone, password);
        //生成token
        String token = jwtConfig.generateToken(userId);
        Map<String, Object> data = Maps.newHashMap();
        data.put("token", token);
        data.put("expire", jwtConfig.getExpire());
        Map<String, Object> map = Maps.newHashMap();
        map.put("data", data);
        return R.ok(map);
    }

}

