package com.aviel.core.app.controller;

import com.aviel.core.app.annotation.Login;
import com.aviel.core.app.service.OrderCleanService;
import com.aviel.core.app.util.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/app/clean")
@Slf4j
public class ApiOrderCleanController {

    @Autowired
    private OrderCleanService orderCleanService;

    @Login
    @PostMapping("/create")
    public R suggest(
            @RequestParam(value = "orderId", required = true) String orderId,
            @RequestParam(value = "cleanPics", required = true) String cleanPics,
            @RequestParam(value = "num", required = true) String num) {
        orderCleanService.create(orderId, cleanPics, num);
        return R.ok();
    }
}
