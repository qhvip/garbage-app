package com.aviel.core.app.controller;

import com.aviel.core.app.annotation.Login;
import com.aviel.core.app.dto.OrderDTO;
import com.aviel.core.app.model.OrderItem;
import com.aviel.core.app.service.OrderService;
import com.aviel.core.app.util.R;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author qhliu
 */
@RestController
@RequestMapping("/app/order")
@Slf4j
public class ApiOrderController {

    @Autowired
    private OrderService orderService;

    @Login
    @PostMapping("/list")
    public R list(
            @RequestAttribute("userId") String userId,
            @RequestParam(value = "showType", defaultValue = "0") Integer showType,
            @RequestParam(value = "queryDate", required = false) String queryDate,
            @RequestParam(value = "orderType", required = false) Integer orderType,
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size) {
        PageInfo<OrderDTO> pageInfo = orderService.findList(userId, showType, queryDate, orderType, page, size);
        Map<String, Object> data = Maps.newHashMap();
        data.put("list", pageInfo.getList());
        data.put("pageNum", pageInfo.getPageNum());
        data.put("pageSize",pageInfo.getPageSize());
        data.put("pages", pageInfo.getPages());
        data.put("total", pageInfo.getTotal());
        Map<String, Object> map = Maps.newHashMap();
        map.put("data", data);
        return R.ok(map);
    }

    @Login
    @PostMapping("/detail")
    public R detail(@RequestParam(value = "orderId", required = true) String orderId) {
        OrderDTO orderDTO = orderService.queryByOrderId(orderId);
        Map<String, Object> map = Maps.newHashMap();
        map.put("data", orderDTO);
        return R.ok(map);
    }

    @Login
    @PostMapping("/cancel")
    public R cancel(
            @RequestAttribute("userId") String userId,
            @RequestParam(value = "orderId", required = true) String orderId,
            @RequestParam(value = "reason", required = true) String reason) {
        OrderDTO orderDTO = orderService.queryByOrderId(orderId);
        orderDTO.setCancelReason(reason);
        orderService.cancel(orderDTO);
        return R.ok();
    }

    @Login
    @PostMapping("/receive")
    public R receive(@RequestParam(value = "orderId", required = true) String orderId) {
        OrderDTO orderDTO = orderService.queryByOrderId(orderId);
        orderService.receive(orderDTO);
        return R.ok();
    }

    @Login
    @PostMapping("/finish")
    public R finished(
            @RequestAttribute("userId") String userId,
            @RequestParam(value = "orderId", required = true) String orderId,
            @RequestParam(value = "items", required = true) String items,
            @RequestParam(value = "addressType", required = false) Integer addressType) {
        OrderDTO orderDTO = orderService.queryByOrderId(orderId);
        orderDTO.setAddressType(addressType);
        List<OrderItem> orderItemList = Lists.newArrayList();
        try {
            orderItemList = new Gson().fromJson(items, new TypeToken<List<OrderItem>>() {}.getType());
        } catch (Exception e) {
            return R.error("参数值出错");
        }
        orderDTO.setOrderItems(orderItemList);
        orderService.finish(orderDTO);
        return R.ok();
    }
}