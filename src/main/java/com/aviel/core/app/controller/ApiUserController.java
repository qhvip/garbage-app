package com.aviel.core.app.controller;

import com.aviel.core.app.annotation.Login;
import com.aviel.core.app.dto.UserInfo;
import com.aviel.core.app.exception.HandlerException;
import com.aviel.core.app.model.AppUser;
import com.aviel.core.app.service.UserService;
import com.aviel.core.app.util.R;
import com.google.common.collect.Maps;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author qhliu
 */
@RestController
@RequestMapping("/app/user")
public class ApiUserController {

    @Autowired
    private UserService userService;

    @Login
    @PostMapping("/info")
    public R userInfo(@RequestAttribute("userId") String userId) {
        AppUser user = userService.queryByUserId(userId);
        if (user == null) {
            return R.error(HttpStatus.UNAUTHORIZED.value(), "用户不存在");
        }
        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(user.getUserId());
        userInfo.setUsername(user.getUsername());
        userInfo.setPhone(user.getPhone());
        userInfo.setAvatarUrl(user.getAvatarUrl());
        userInfo.setCompany(user.getCompany());
        Map<String, Object> map = Maps.newHashMap();
        map.put("data", userInfo);
        return R.ok(map);
    }

    @Login
    @PostMapping("/reset/password")
    public R password(
            @RequestAttribute("userId") String userId,
            @RequestParam(value = "password") String password,
            @RequestParam(value = "newPassword") String newPassword){
        if (StringUtils.isBlank(password)) {
            throw new HandlerException(HttpStatus.BAD_REQUEST.value(), "原密码不能为空");
        }
        if (StringUtils.isBlank(newPassword)) {
            throw new HandlerException(HttpStatus.BAD_REQUEST.value(), "新密码不为能空");
        }
        //sha256加密
        String encodePassword = DigestUtils.sha256Hex(password);
        //sha256加密
        String encodeNewPassword = DigestUtils.sha256Hex(newPassword);
        //更新密码
        boolean result = userService.updatePassword(userId, encodePassword, encodeNewPassword);
        if (!result) {
            return R.error("未知错误");
        }
        return R.ok();
    }
}