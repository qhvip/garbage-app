package com.aviel.core.app.controller;

import com.aviel.core.app.annotation.Login;
import com.aviel.core.app.model.OrderProcess;
import com.aviel.core.app.service.OrderProcessService;
import com.aviel.core.app.util.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/app/process")
@Slf4j
public class ApiOrderProcessController {

    @Autowired
    private OrderProcessService orderProcessService;

    @Login
    @PostMapping("/update")
    public R suggest(@RequestParam(value = "orderId", required = true) String orderId) {
        OrderProcess orderProcess = new OrderProcess();
        orderProcess.setOrderId(orderId);
        orderProcess.setIsRead(1);
        orderProcess.setUpdateTime(new Date());
        orderProcessService.updateOrderProcess(orderProcess);
        return R.ok();
    }
}