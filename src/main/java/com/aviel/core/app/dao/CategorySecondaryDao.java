package com.aviel.core.app.dao;

import com.aviel.core.app.model.CategorySecondary;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CategorySecondaryDao {

    CategorySecondary selectById(@Param("id") Long id);

    List<CategorySecondary> selectCategorySecondaryListByCid(@Param("cid") Long cid);
}