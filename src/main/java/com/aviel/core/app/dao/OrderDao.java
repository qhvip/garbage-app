package com.aviel.core.app.dao;

import com.aviel.core.app.model.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderDao {

    int updateOrder(@Param("order") Order order);

    Order selectByOrderId(@Param("orderId") String orderId);

    List<Order> selectByDeliverId(
            @Param("deliverId") String deliverId,
            @Param("showType") Integer showType,
            @Param("queryDate") String queryDate,
            @Param("orderType") Integer orderType
    );
}