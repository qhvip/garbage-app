package com.aviel.core.app.dao;

import com.aviel.core.app.model.OrderItem;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderItemDao {

    int insertOrderItem(@Param("orderItem") OrderItem orderItem);

    int deleteOrderItem(@Param("orderId") String orderId);

    int updateOrderItem(@Param("orderItem") OrderItem orderItem);

    List<OrderItem> selectByOrderId(@Param("orderId") String orderId);
}