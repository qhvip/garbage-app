package com.aviel.core.app.dao;

import com.aviel.core.app.model.OrderFeedback;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderFeedbackDao {

    int insertOrderFeedback(@Param("orderFeedback") OrderFeedback orderFeedback);

    int updateOrderFeedback(@Param("orderFeedback") OrderFeedback orderFeedback);

    List<OrderFeedback> selectByOrderId(@Param("orderId") String orderId);
}