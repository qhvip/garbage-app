package com.aviel.core.app.dao;

import com.aviel.core.app.model.OrderProcess;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderProcessDao {

    List<OrderProcess> selectListByDeliverId(@Param("deliverId") String deliverId);

    int updateOrderProcess(@Param("orderProcess") OrderProcess orderProcess);

    int deleteByOrderId(@Param("orderId") String orderId);
}