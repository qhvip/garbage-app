package com.aviel.core.app.dao;

import com.aviel.core.app.model.Item;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ItemDao {

    Item selectById(@Param("id") Long id);

    List<Item> selectByCatId(@Param("catId") Long catId);
}