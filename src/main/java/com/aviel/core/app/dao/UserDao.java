package com.aviel.core.app.dao;

import com.aviel.core.app.model.AppUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UserDao {

    int insertAppUser(@Param("user") AppUser user);

    int deleteById(String userId);

    int updateAppUser(@Param("user") AppUser user);

    int updateAppUserPassword(@Param("user") AppUser user);

    AppUser selectById(String userId);

    AppUser selectByPhone(String phone);

    List<AppUser> selectList(Map<String, Object> map);
}
