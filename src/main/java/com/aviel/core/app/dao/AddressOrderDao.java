package com.aviel.core.app.dao;

import com.aviel.core.app.model.AddressOrder;
import org.apache.ibatis.annotations.Param;

public interface AddressOrderDao {

    int updateById(@Param("addressOrder") AddressOrder addressOrder);

    AddressOrder selectById(@Param("id") Long id);
}