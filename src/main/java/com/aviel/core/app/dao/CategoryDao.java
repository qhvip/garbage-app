package com.aviel.core.app.dao;

import com.aviel.core.app.model.Category;
import org.apache.ibatis.annotations.Param;

public interface CategoryDao {

    Category selectById(@Param("id") Long id);
}