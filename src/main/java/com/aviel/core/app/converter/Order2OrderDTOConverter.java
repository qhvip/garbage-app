package com.aviel.core.app.converter;

import com.aviel.core.app.dto.OrderDTO;
import com.aviel.core.app.model.Order;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author qhliu
 */
public class Order2OrderDTOConverter {

    public static OrderDTO convert(Order order) {

        OrderDTO orderDTO = new OrderDTO();
        BeanUtils.copyProperties(order, orderDTO);
        if (StringUtils.isEmpty(order.getImages())) {
            orderDTO.setImages(new JsonArray());
        } else {
            orderDTO.setImages(new JsonParser().parse(order.getImages()).getAsJsonArray());
        }
        return orderDTO;
    }

    public static List<OrderDTO> convert(List<Order> orderList) {
        return orderList.stream().map(e ->
                convert(e)
        ).collect(Collectors.toList());
    }
}
