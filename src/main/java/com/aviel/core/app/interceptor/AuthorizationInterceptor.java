package com.aviel.core.app.interceptor;

import com.aviel.core.app.annotation.Login;
import com.aviel.core.app.config.JwtConfig;
import com.aviel.core.app.exception.HandlerException;
import io.jsonwebtoken.Claims;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class AuthorizationInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    private JwtConfig jwtConfig;

    public static final String USER_KEY = "userId";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Login loginAnnotation;
        if (handler instanceof HandlerMethod) {
            loginAnnotation = ((HandlerMethod) handler).getMethodAnnotation(Login.class);
        } else {
            return true;
        }
        if (loginAnnotation == null) {
            return true;
        }
        //获取用户凭证
        String token = request.getHeader(jwtConfig.getHeader());
        if (StringUtils.isBlank(token)) {
            token = request.getParameter(jwtConfig.getHeader());
        }

        //凭证为空
        if (StringUtils.isBlank(token)) {
            throw new HandlerException(HttpStatus.UNAUTHORIZED.value(), jwtConfig.getHeader() + "不能为空");
        }

        Claims claims = jwtConfig.getClaimByToken(token);
        if (claims == null || jwtConfig.isTokenExpired(claims.getExpiration())) {
            throw new HandlerException(HttpStatus.UNAUTHORIZED.value(), jwtConfig.getHeader() + "失效，请重新登录");
        }

        //设置userId到request里，后续根据userId，获取用户信息
        request.setAttribute(USER_KEY, claims.getSubject());

        return true;
    }
}

