package com.aviel.core.app.exception;

import com.aviel.core.app.util.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

@RestControllerAdvice
public class HandlerExceptionAdvice {
    private Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 处理自定义异常
     */
    @ExceptionHandler(HandlerException.class)
    public R handlerException(HandlerException e) {
        R r = new R();
        r.put("code", e.getCode());
        r.put("msg", e.getMessage());
        r.put("data", null);
        return r;
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public R handlerIllegalArgumentException(IllegalArgumentException e) {
        logger.error(e.getMessage(), e);
        return R.error(402, "参数值出错");
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public R handlerNoFoundException(NoHandlerFoundException e) {
        logger.error(e.getMessage(), e);
        return R.error(404, "路径不存在，请检查路径是否正确");
    }

    @ExceptionHandler(DuplicateKeyException.class)
    public R handleDuplicateKeyException(DuplicateKeyException e) {
        logger.error(e.getMessage(), e);
        return R.error("数据库中已存在该记录");
    }

    @ExceptionHandler(Exception.class)
    public R handleException(Exception e) {
        logger.error(e.getMessage(), e);
        return R.error();
    }

}
