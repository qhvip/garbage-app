package com.aviel.core.app.exception;

import com.aviel.core.app.enums.ResultEnum;

public class HandlerException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private int code = 500;
    private String msg;

    public HandlerException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public HandlerException(String msg, Throwable e) {
        super(msg, e);
        this.msg = msg;
    }

    public HandlerException(int code, String msg) {
        super(msg);
        this.code = code;
        this.msg = msg;
    }

    public HandlerException(int code, String msg, Throwable e) {
        super(msg, e);
        this.code = code;
        this.msg = msg;
    }

    public HandlerException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMessage();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}

