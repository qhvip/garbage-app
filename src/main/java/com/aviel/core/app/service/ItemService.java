package com.aviel.core.app.service;

import com.aviel.core.app.dao.ItemDao;
import com.aviel.core.app.dto.ItemDTO;
import com.aviel.core.app.enums.ResultEnum;
import com.aviel.core.app.exception.HandlerException;
import com.aviel.core.app.model.Item;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ItemService {

    @Autowired
    private ItemDao itemDao;

    public Item queryItem(Long itemId) {
        return itemDao.selectById(itemId);
    }

    public ItemDTO queryByItemId(Long itemId) {
        Item item = queryItem(itemId);
        if (item == null) {
            throw new HandlerException(ResultEnum.PRODUCT_NOT_EXIST);
        }
        ItemDTO itemDTO = new ItemDTO();
        BeanUtils.copyProperties(item, itemDTO);
        return itemDTO;
    }

    public List<ItemDTO> queryByCatId(Long catId) {
        List<Item> itemList = itemDao.selectByCatId(catId);
        if (CollectionUtils.isEmpty(itemList)) {
            return Collections.emptyList();
        }
        return itemList.stream().map(ad -> {
            ItemDTO itemDTO = new ItemDTO();
            BeanUtils.copyProperties(ad, itemDTO);
            return itemDTO;
        }).collect(Collectors.toList());
    }
}