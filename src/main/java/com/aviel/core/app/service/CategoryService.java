package com.aviel.core.app.service;

import com.aviel.core.app.dao.CategoryDao;
import com.aviel.core.app.dao.CategorySecondaryDao;
import com.aviel.core.app.dto.CategoryDTO;
import com.aviel.core.app.dto.CategorySecondaryDTO;
import com.aviel.core.app.enums.ResultEnum;
import com.aviel.core.app.exception.HandlerException;
import com.aviel.core.app.model.Category;
import com.aviel.core.app.model.CategorySecondary;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryService {

    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private CategorySecondaryDao categorySecondaryDao;

    public Category queryCategory(Long id) {
        Category category = categoryDao.selectById(id);
        if (category == null) {
            throw new HandlerException(ResultEnum.PRODUCT_CATRGORY_EMPTY);
        }
        return category;
    }

    public CategoryDTO queryByCategoryId(Long id) {
        Category category = queryCategory(id);
        CategoryDTO categoryDTO = new CategoryDTO();
        BeanUtils.copyProperties(category, categoryDTO);
        return categoryDTO;
    }

    public CategorySecondary queryCategorySecondary(Long id) {
        CategorySecondary categorySecondary = categorySecondaryDao.selectById(id);
        if (categorySecondary == null) {
            throw new HandlerException(ResultEnum.PRODUCT_CATRGORY_EMPTY);
        }
        return categorySecondary;
    }

    public CategorySecondaryDTO queryByCategorySecondaryId(Long id) {
        CategorySecondary categorySecondary = queryCategorySecondary(id);
        CategorySecondaryDTO categorySecondaryDTO = new CategorySecondaryDTO();
        BeanUtils.copyProperties(categorySecondary, categorySecondaryDTO);
        return categorySecondaryDTO;
    }

    public List<CategorySecondaryDTO> queryCategorySecondaryList(Long cid) {
        List<CategorySecondary> categorySecondaryList = categorySecondaryDao.selectCategorySecondaryListByCid(cid);
        if (CollectionUtils.isEmpty(categorySecondaryList)) {
            return Collections.emptyList();
        }
        return categorySecondaryList.stream().map(categorySecondary -> {
            CategorySecondaryDTO categorySecondaryDTO = new CategorySecondaryDTO();
            BeanUtils.copyProperties(categorySecondary, categorySecondaryDTO);
            return categorySecondaryDTO;
        }).collect(Collectors.toList());
    }
}