package com.aviel.core.app.service;

import com.aviel.core.app.model.AppUser;
import com.aviel.core.app.dao.UserDao;
import com.aviel.core.app.exception.HandlerException;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Objects;

@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    public AppUser queryByUserId(String userId) {
        return userDao.selectById(userId);
    }

    public AppUser queryByMobile(String phone) {
        return userDao.selectByPhone(phone);
    }

    public String login(String phone, String password) {
        AppUser user = queryByMobile(phone);
        Assert.notNull(user, "用户不存在");

        //密码错误
        if (!user.getPassword().equals(DigestUtils.sha256Hex(password))) {
            throw new HandlerException("手机号或密码错误");
        }
        return user.getUserId();
    }

    public void save(AppUser user) {
        userDao.insertAppUser(user);
    }

    public boolean updatePassword(String userId, String password, String newPassword) {
        AppUser user = queryByUserId(userId);
        if (!Objects.equals(password, user.getPassword())) {
            throw new HandlerException("原密码不正确");
        }
        user.setPassword(newPassword);
        userDao.updateAppUserPassword(user);
        return true;
    }
}
