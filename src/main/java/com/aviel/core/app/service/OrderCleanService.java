package com.aviel.core.app.service;

import com.aviel.core.app.dao.OrderFeedbackDao;
import com.aviel.core.app.dto.OrderDTO;
import com.aviel.core.app.dto.OrderFeedbackDTO;
import com.aviel.core.app.enums.OrderStatusEnum;
import com.aviel.core.app.enums.ResultEnum;
import com.aviel.core.app.exception.HandlerException;
import com.aviel.core.app.model.Order;
import com.aviel.core.app.model.OrderFeedback;
import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderCleanService {

    @Autowired
    private OrderFeedbackDao orderFeedbackDao;
    @Autowired
    private OrderService orderService;

    @Transactional(rollbackFor = Exception.class)
    public void create(String orderId, String cleanPics, String num) {
        orderService.queryByOrderId(orderId);
        Order order = new Order();
        order.setOrderId(orderId);
        order.setOrderStatus(OrderStatusEnum.FINISHED.getCode());
        order.setCompleteTime(new Date());
        order.setUpdateTime(new Date());
        if (!orderService.updateOrder(order)) {
            throw new HandlerException(ResultEnum.ORDER_UPDATE_FAIL);
        }
        List<OrderFeedback> orderFeedbackList = queryByOrderId(orderId);
        if (CollectionUtils.isEmpty(orderFeedbackList)) {
            saveOrderFeedback(orderId, cleanPics, num, 1, 0);
        } else {
            OrderFeedback maxLevelOrderFeedback = orderFeedbackList.stream().max(Comparator.comparing(OrderFeedback::getLevel)).get();
            if (maxLevelOrderFeedback != null) {
                if (maxLevelOrderFeedback.getStatus() != 0) {
                    saveOrderFeedback(orderId, cleanPics, num, maxLevelOrderFeedback.getLevel() + 1, 0);
                } else {
                    maxLevelOrderFeedback.setCleanPics(cleanPics);
                    maxLevelOrderFeedback.setNum(num);
                    updateOrderFeedback(maxLevelOrderFeedback);
                }
            }
        }
    }

    public List<OrderFeedback> queryByOrderId(String orderId) {
        return orderFeedbackDao.selectByOrderId(orderId);
    }

    public List<OrderFeedbackDTO> queryOrderFeedbackByOrderId(String orderId) {
        List<OrderFeedbackDTO> orderFeedbackDTOList = Lists.newArrayList();
        List<OrderFeedback> orderFeedbackList = queryByOrderId(orderId);
        if (!CollectionUtils.isEmpty(orderFeedbackList)) {
            orderFeedbackDTOList = orderFeedbackList.stream().map(orderFeedback -> {
                OrderFeedbackDTO orderFeedbackDTO = new OrderFeedbackDTO();
                if (StringUtils.isEmpty(orderFeedback.getCleanPics())) {
                    orderFeedbackDTO.setCleanPics(new JsonArray());
                } else {
                    orderFeedbackDTO.setCleanPics(new JsonParser().parse(orderFeedback.getCleanPics()).getAsJsonArray());
                }
                orderFeedbackDTO.setDescription(orderFeedback.getDescription());
                if (StringUtils.isEmpty(orderFeedback.getFeedbackPics())) {
                    orderFeedbackDTO.setFeedbackPics(new JsonArray());
                } else {
                    orderFeedbackDTO.setFeedbackPics(new JsonParser().parse(orderFeedback.getFeedbackPics()).getAsJsonArray());
                }
                orderFeedbackDTO.setNum(orderFeedback.getNum());
                orderFeedbackDTO.setLevel(orderFeedback.getLevel());
                return orderFeedbackDTO;
            }).collect(Collectors.toList());
            orderFeedbackDTOList.sort(Comparator.comparing(OrderFeedbackDTO::getLevel));
        }
        return orderFeedbackDTOList;
    }

    public void updateOrderFeedback(OrderFeedback orderFeedback) {
        orderFeedbackDao.updateOrderFeedback(orderFeedback);
    }

    private void saveOrderFeedback(String orderId, String cleanPics, String num, Integer level, Integer status) {
        OrderFeedback orderFeedback = new OrderFeedback();
        orderFeedback.setOrderId(orderId);
        orderFeedback.setCleanPics(cleanPics);
        orderFeedback.setNum(num);
        orderFeedback.setLevel(level);
        orderFeedback.setStatus(status);
        orderFeedback.setCreateTime(new Date());
        orderFeedbackDao.insertOrderFeedback(orderFeedback);
    }
}
