package com.aviel.core.app.service;

import com.aviel.core.app.dao.AddressOrderDao;
import com.aviel.core.app.enums.ResultEnum;
import com.aviel.core.app.exception.HandlerException;
import com.aviel.core.app.model.AddressOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressOrderService {

    @Autowired
    private AddressOrderDao addressOrderDao;

    public int updateById(AddressOrder addressOrder) {
        return addressOrderDao.updateById(addressOrder);
    }

    public AddressOrder queryById(Long id) {
        AddressOrder addressOrder = addressOrderDao.selectById(id);
        if (addressOrder == null) {
            throw new HandlerException("获取订单地址失败");
        }
        return addressOrder;
    }
}