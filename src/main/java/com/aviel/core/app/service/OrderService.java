package com.aviel.core.app.service;

import com.aviel.core.app.converter.Order2OrderDTOConverter;
import com.aviel.core.app.dao.OrderDao;
import com.aviel.core.app.dao.OrderItemDao;
import com.aviel.core.app.dto.OrderDTO;
import com.aviel.core.app.dto.OrderFeedbackDTO;
import com.aviel.core.app.enums.OrderStatusEnum;
import com.aviel.core.app.enums.ResultEnum;
import com.aviel.core.app.exception.HandlerException;
import com.aviel.core.app.model.*;
import com.aviel.core.app.util.AsyncThreadUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class OrderService {

    @Autowired
    private OrderDao orderDao;
    @Autowired
    private OrderItemDao orderItemDao;
    @Autowired
    private ItemService itemService;
    @Autowired
    private AddressOrderService addressOrderService;
    @Autowired
    private OrderCleanService orderCleanService;
    @Autowired
    private OrderProcessService orderProcessService;

    public PageInfo<OrderDTO> findList(String userId, Integer showType, String queryDate, Integer orderType, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Order> orderList = orderDao.selectByDeliverId(userId, showType, queryDate, orderType);
        List<OrderDTO> orderDTOList = Order2OrderDTOConverter.convert(orderList);
        for (OrderDTO orderDTO : orderDTOList) {
            List<OrderItem> orderItemList = orderItemDao.selectByOrderId(orderDTO.getOrderId());
            orderDTO.setOrderItems(orderItemList);
        }
        PageInfo<OrderDTO> pageInfo = new PageInfo<OrderDTO>(orderDTOList);
        return pageInfo;
    }

    public OrderDTO findOne(String userId, String orderId) {
        return checkOrderOwner(userId, orderId);
    }

    @Transactional(rollbackFor = Exception.class)
    public void cancel(OrderDTO orderDTO) {
        if (!OrderStatusEnum.RECEIVE.getCode().equals(orderDTO.getOrderStatus())) {
            throw new HandlerException(ResultEnum.ORDER_STATUS_ERROR);
        }
        Order order = new Order();
        order.setOrderId(orderDTO.getOrderId());
        order.setCancelReason(orderDTO.getCancelReason());
        order.setOrderStatus(OrderStatusEnum.CANCEL.getCode());
        order.setCancelTime(new Date());
        order.setUpdateTime(new Date());
        if (!updateOrder(order)) {
            throw new HandlerException(ResultEnum.ORDER_UPDATE_FAIL);
        }
        AsyncThreadUtil.execute(() -> {
            orderProcessService.deleteOrderProcess(orderDTO.getOrderId());
        });
    }

    @Transactional(rollbackFor = Exception.class)
    public void receive(OrderDTO orderDTO) {
        if (!OrderStatusEnum.APPORTIONED.getCode().equals(orderDTO.getOrderStatus())) {
            throw new HandlerException(ResultEnum.ORDER_STATUS_ERROR);
        }
        Order order = new Order();
        order.setOrderId(orderDTO.getOrderId());
        order.setOrderStatus(OrderStatusEnum.RECEIVE.getCode());
        order.setReceiveTime(new Date());
        order.setUpdateTime(new Date());
        if (!updateOrder(order)) {
            throw new HandlerException(ResultEnum.ORDER_UPDATE_FAIL);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void finish(OrderDTO orderDTO) {
        if (!OrderStatusEnum.RECEIVE.getCode().equals(orderDTO.getOrderStatus())) {
            throw new HandlerException(ResultEnum.ORDER_STATUS_ERROR);
        }
        if (orderDTO.getAddressType() != null) {
            AddressOrder addressOrder = new AddressOrder();
            addressOrder.setId(orderDTO.getAddressOrderId());
            addressOrder.setAddressType(orderDTO.getAddressType());
            addressOrderService.updateById(addressOrder);
        }
        orderItemDao.deleteOrderItem(orderDTO.getOrderId());
        BigDecimal orderAmount = orderDTO.getOrderAmount();
        List<OrderItem> orderItemList = orderDTO.getOrderItems();
        if (orderItemList != null && orderItemList.size() > 0) {
            for (OrderItem orderItem : orderItemList) {
                Item item = itemService.queryItem(orderItem.getItemId());
                if (item == null) {
                    throw new HandlerException(ResultEnum.PRODUCT_NOT_EXIST);
                }
                orderAmount = item.getPrice()
                        .multiply(new BigDecimal(orderItem.getItemNum()))
                        .add(orderAmount);
                orderItem.setOrderId(orderDTO.getOrderId());
                BeanUtils.copyProperties(item, orderItem);
                orderItem.setCreateTime(new Date());
                orderItem.setUpdateTime(new Date());
                orderItemDao.insertOrderItem(orderItem);
            }
        }
        Order order = new Order();
        order.setOrderId(orderDTO.getOrderId());
        order.setOrderAmount(orderAmount);
        order.setOrderStatus(OrderStatusEnum.FINISHED.getCode());
        order.setCompleteTime(new Date());
        order.setUpdateTime(new Date());
        if (!updateOrder(order)) {
            throw new HandlerException(ResultEnum.ORDER_UPDATE_FAIL);
        }
        AsyncThreadUtil.execute(() -> {
            orderProcessService.deleteOrderProcess(orderDTO.getOrderId());
        });
    }

    public boolean updateOrder(Order order) {
        int count = orderDao.updateOrder(order);
        if (count > 0) {
            return true;
        }
        return false;
    }

    public OrderDTO queryByOrderId(String orderId) {
        Order order = orderDao.selectByOrderId(orderId);
        if (order == null) {
            throw new HandlerException(ResultEnum.ORDER_NOT_EXIST);
        }
        List<OrderItem> orderItemList = orderItemDao.selectByOrderId(orderId);
        if (CollectionUtils.isEmpty(orderItemList)) {
            throw new HandlerException(ResultEnum.ORDER_DETAIL_EMPTY);
        }
        List<OrderFeedbackDTO> orderFeedbackDTOList = orderCleanService.queryOrderFeedbackByOrderId(orderId);
        OrderDTO orderDTO = new OrderDTO();
        BeanUtils.copyProperties(order, orderDTO);
        if (StringUtils.isEmpty(order.getImages())) {
            orderDTO.setImages(new JsonArray());
        } else {
            orderDTO.setImages(new JsonParser().parse(order.getImages()).getAsJsonArray());
        }
        orderDTO.setOrderItems(orderItemList);
        orderDTO.setOrderFeedbacks(CollectionUtils.isEmpty(orderFeedbackDTOList) ? Collections.emptyList() : orderFeedbackDTOList);
        return orderDTO;
    }

    private OrderDTO checkOrderOwner(String userId, String orderId) {
        OrderDTO orderDTO = queryByOrderId(orderId);
        if (orderDTO == null) {
            return null;
        }
        if (!userId.equals(orderDTO.getDeliverId())) {
            throw new HandlerException(ResultEnum.ORDER_OWNER_ERROR);
        }
        return orderDTO;
    }
}