package com.aviel.core.app.service;

import com.aviel.core.app.dao.OrderProcessDao;
import com.aviel.core.app.model.OrderProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderProcessService {

    @Autowired
    private OrderProcessDao orderProcessDao;

    public List<OrderProcess> queryList(String deliverId) {
        return orderProcessDao.selectListByDeliverId(deliverId);
    }

    public int updateOrderProcess(OrderProcess orderProcess) {
        return orderProcessDao.updateOrderProcess(orderProcess);
    }

    public int deleteOrderProcess(String orderId) {
        return orderProcessDao.deleteByOrderId(orderId);
    }
}