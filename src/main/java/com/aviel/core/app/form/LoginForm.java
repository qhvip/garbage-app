package com.aviel.core.app.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class LoginForm {
    @NotBlank(message="手机号不能为空")
    private String phone;

    @NotBlank(message="密码不能为空")
    private String password;
}

