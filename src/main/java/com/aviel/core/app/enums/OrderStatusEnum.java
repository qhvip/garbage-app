package com.aviel.core.app.enums;

import lombok.Getter;

@Getter
public enum OrderStatusEnum implements CodeEnum {
    NEW(0, "新订单"),
    APPORTIONED(1, "已分配"),
    RECEIVE(2, "已接单"),
    FINISHED(3, "已完成"),
    CANCEL(4, "已取消");

    private Integer code;

    private String message;

    OrderStatusEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
