package com.aviel.core.app.enums;

public interface CodeEnum {
    Integer getCode();
}