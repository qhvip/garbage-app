package com.aviel.core.app.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class OrderProcess {

    private Long id;
    private String orderId;
    private String deliverId;
    private String title;
    private Integer orderType;
    private String content;
    private Integer orderStatus;
    /**
     * 0:新订单 1:超时 2:反馈 3：催办
     */
    private Integer status;
    private Integer isRead;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}