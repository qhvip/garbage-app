package com.aviel.core.app.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class Order {

    private String orderId;
    private Long userId;
    private Long addressOrderId;
    private String scheduledDate;
    private String scheduledTime;
    private String num;
    private BigDecimal orderAmount;
    private Integer orderType;
    private Integer orderStatus;
    private String images;
    private String remark;
    private Integer isTransport;//驳运
    private String deliverId;
    private String deliverName;
    private String deliverTel;
    private Integer isReminder;//催办
    private Integer isFeedback;//反馈
    private String cancelReason;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date cancelTime;//取消时间
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date dispatchTime;//派单时间
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date receiveTime;//接单时间
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date completeTime;//完成时间
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date expireTime;//过期时间
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;//创建时间
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;//更新时间
    private String name;
    private String tel;
    private String provinceName;
    private String cityName;
    private String countyName;
    private String townName;
    private String villageName;
    private String fullAddress;
    private Integer addressType;
}