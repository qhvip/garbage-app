package com.aviel.core.app.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class OrderItem {

    private Long id;
    private Long itemId;
    private Integer itemNum;
    private String title;
    private String orderId;
    private BigDecimal price;
    private String image;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}