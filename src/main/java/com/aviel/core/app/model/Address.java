package com.aviel.core.app.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class Address {

    private Long id;
    private Long userId;
    private String name;
    private String tel;
    private String provinceCode;
    private String provinceName;
    private String cityCode;
    private String cityName;
    private String countyCode;
    private String countyName;
    private String townCode;
    private String townName;
    private String villageCode;
    private String villageName;
    private String addressDetail;
    private String fullAddress;
    private Integer addressType;
    private Integer isDefault;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}