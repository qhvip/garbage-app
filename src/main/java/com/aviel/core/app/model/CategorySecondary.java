package com.aviel.core.app.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class CategorySecondary {

    private Long id;
    private Long cid;
    private String name;
    private Integer status;
    private Integer sortOrder;
    private Integer isParent;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}