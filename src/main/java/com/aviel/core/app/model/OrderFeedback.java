package com.aviel.core.app.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class OrderFeedback {

    public Long id;
    private String orderId;
    private String feedbackPics;
    private String description;
    private String cleanPics;
    private String num;
    private Integer level;
    private Integer status;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
