package com.aviel.core.app.model;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class AddressOrder {

    private Long id;
    private Long addressId;
    private Long userId;
    private String name;
    private String tel;
    private String provinceCode;
    private String provinceName;
    private String cityCode;
    private String cityName;
    private String countyCode;
    private String countyName;
    private String townCode;
    private String townName;
    private String villageCode;
    private String villageName;
    private String fullAddress;
    private Integer addressType;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}