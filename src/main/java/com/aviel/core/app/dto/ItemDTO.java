package com.aviel.core.app.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ItemDTO {

    private Long id;
    private String title;
    private BigDecimal price;
    private String image;
}