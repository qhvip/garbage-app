package com.aviel.core.app.dto;

import com.google.gson.JsonArray;
import lombok.Data;

@Data
public class OrderFeedbackDTO {

    private JsonArray feedbackPics;
    private String description;
    private JsonArray cleanPics;
    private String num;
    private Integer level;
}
