package com.aviel.core.app.dto;

import lombok.Data;

@Data
public class CategoryDTO {

    private Long id;
    private String name;
    private Integer sortOrder;
}