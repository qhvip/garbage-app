package com.aviel.core.app.dto;

import lombok.Data;

@Data
public class UserInfo {
    private String userId;
    private String username;
    private String phone;
    private String avatarUrl;
    private String company;
}