package com.aviel.core.app.dto;

import com.aviel.core.app.model.OrderItem;
import com.google.gson.JsonArray;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class OrderDTO {

    private String orderId;
    private Integer orderType;
    private Long userId;
    private Long addressOrderId;
    private String scheduledDate;
    private String scheduledTime;
    private String num;
    private BigDecimal orderAmount;
    private JsonArray images;
    private String remark;
    private Integer orderStatus;
    private String townName;//街道
    private String villageName;//居委
    private Integer isTransport;//驳运
    private String deliverId;
    private String deliverName;
    private String deliverTel;
    private Integer isReminder;//催办
    private Integer isFeedback;//反馈
    private String repairDeclareOrderNo;//装修申报订单号
    private String cancelReason;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date cancelTime;//取消时间
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date dispatchTime;//派单时间
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date receiveTime;//接单时间
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date completeTime;//完成时间
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date expireTime;//过期时间
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;//创建时间
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;//更新时间
    private List<OrderItem> orderItems;
    private String name;
    private String tel;
    private String fullAddress;
    private Integer addressType;
    private List<OrderFeedbackDTO> orderFeedbacks;
}
