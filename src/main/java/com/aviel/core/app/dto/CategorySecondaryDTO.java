package com.aviel.core.app.dto;

import lombok.Data;

@Data
public class CategorySecondaryDTO {

    private Long id;
    private Long parentId;
    private String name;
    private Integer status;
    private Integer sortOrder;
    private Integer isParent;
}