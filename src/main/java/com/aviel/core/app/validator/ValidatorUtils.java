package com.aviel.core.app.validator;

import com.aviel.core.app.exception.HandlerException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

public class ValidatorUtils {
    private static Validator validator;

    static {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    /**
     * 校验对象
     * @param object        待校验对象
     * @param groups        待校验的组
     * @throws HandlerException  校验不通过，则报HandlerException异常
     */
    public static void validateEntity(Object object, Class<?>... groups) throws HandlerException {
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object, groups);
        if (!constraintViolations.isEmpty()) {
            StringBuilder msg = new StringBuilder();
            for(ConstraintViolation<Object> constraint:  constraintViolations){
                msg.append(constraint.getMessage()).append("\n");
            }
            throw new HandlerException(msg.toString());
        }
    }
}

