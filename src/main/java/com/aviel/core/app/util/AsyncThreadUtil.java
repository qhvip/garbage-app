package com.aviel.core.app.util;

import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;

@Slf4j
public class AsyncThreadUtil {

    private static final int corePoolSize = 10;

    private static final int maxPoolSize = corePoolSize * 2;

    private static final int maxQueueSize = maxPoolSize * 10;
    /**
     * 平均每秒任务数量 * 平均执行等待秒数 / 平均可接受任务处理秒数
     * <p>
     * 假设每秒1000个任务
     */
    private static final ExecutorService executorService = new ThreadPoolExecutor(
            corePoolSize, maxPoolSize, 60L, TimeUnit.SECONDS,
            new LinkedBlockingQueue<Runnable>(maxQueueSize),
            new CustomRejectedPolicy());

    public static void execute(Runnable command) {
        executorService.execute(command);
    }

    public static <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) {
        try {
            return executorService.invokeAll(tasks);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> Future<T> submit(Callable<T> task) {
        return executorService.submit(task);
    }

    public static void shutdown() {
        try {
            executorService.shutdown();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public static int getQueueSize() {
        return ((ThreadPoolExecutor) executorService).getQueue().size();
    }

    private static class CustomRejectedPolicy implements RejectedExecutionHandler {
        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            log.info("executorService reject execute: {}", r.getClass().getName());
            if (!executor.isShutdown()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    log.error("thread error.",e);
                }
                executor.execute(r);
            }
        }
    }
}