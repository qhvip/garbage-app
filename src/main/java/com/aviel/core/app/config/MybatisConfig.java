package com.aviel.core.app.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.aviel.core.app.dao")
public class MybatisConfig {
}